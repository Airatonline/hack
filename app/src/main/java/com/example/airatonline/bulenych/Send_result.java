package com.example.airatonline.bulenych;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class Send_result extends AppCompatActivity {
    DBHelper dbHelper;
    Spinner spinner,spinner1;
    EditText text, fullresult;
    ArrayAdapter<String> adapter, adapter1;
    Button btn;
    SMSWorker smsWorker;
    TextView dataInfo;

    String[] state = {"Завершено", "В работе", "Не начато", "Отменена"};
    String[] endResult={"Выполнена", "Инфомация получена", "Найдено решение", "Необходима дополнительная инофмация", "Необходима эскалация", "Отменена", "Перенесена"};

    void initObjects(){
        spinner = findViewById(R.id.spinner);
        spinner1 = findViewById(R.id.spinner1);
        text = findViewById(R.id.editText);
        fullresult = findViewById(R.id.editText2);
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, state);
        adapter1 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, endResult);
        btn = findViewById(R.id.button2);
        dataInfo = findViewById(R.id.dataInfo);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_result);
        initObjects();
        //работы с бд

        //активация работы с смс
        smsWorker = new SMSWorker();



        //инициалзация спинеров, подключение адаптеров
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner1.setAdapter(adapter1);
        spinner.setPrompt("Состояние");

        //инициализация слушателя
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i==1 || i==2){
                    fullresult.setEnabled(false);
                    spinner1.setEnabled(false);
                }
                else {
                    fullresult.setEnabled(true);
                    spinner1.setEnabled(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        dataInfo.setText((getIntent().getStringExtra("Text")).split("&&")[0]);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = getIntent();
                smsWorker.sendSMS("-0000-" +intent.getStringExtra("ID")+"-"+spinner.getSelectedItem().toString()+"-"+
                       spinner1.getSelectedItem().toString()+"-"+ fullresult.getText().toString(), Send_result.this);
                Log.d("in", intent.getStringExtra("ID"));
                Intent intent1 = new Intent(Send_result.this, MainActivity.class);
                dbHelper = new DBHelper(Send_result.this);
                SQLiteDatabase db1 = dbHelper.getWritableDatabase();
                if(!(spinner.getSelectedItem().toString().equals("В работе")|| spinner.getSelectedItem().toString().equals("Не начато")))
                db1.execSQL("DELETE FROM task WHERE id= '"+ intent.getStringExtra("ID")+"'");
                else db1.execSQL("UPDATE task SET status = '" +spinner.getSelectedItem().toString()+ "' WHERE ID = "+intent.getStringExtra("ID"));
                startActivityForResult(intent1, RESULT_OK);
                finish();
            }
        });

    }

}
