package com.example.airatonline.bulenych;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class DataAdapter extends BaseAdapter {
    Context ctx;
    LayoutInflater inflater;
    ArrayList<Item> items;

    DataAdapter(Context context, ArrayList<Item> items){
        ctx = context;
        this.items = items;
        inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }
    Item getElement(int i){
        return ((Item) getItem(i));
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {


        View view1 = view;
        if(view==null){
            view1 = inflater.inflate(R.layout.item, viewGroup, false);
        }
        Item item = getElement(i);
        String[] a = new String[2];
        int p=0;
        Log.d("String", item.getInfoWork());
        for(String d:item.getInfoWork().split("&&")){
            Log.d("String", d);
            a[p]=d;
            p++;
        }


        ((TextView) view1.findViewById(R.id.infoWork)).setText(a[0]);
        ((TextView) view1.findViewById(R.id.infoLong)).setText(a[1]);
        LinearLayout abc = (LinearLayout) view1.findViewById(R.id.background_system);
        if(item.getStatus().equals("средний")) {abc.setBackground(ctx.getDrawable(R.drawable.background_yellow));}
        else if(item.getStatus().equals("высокий")) abc.setBackground(ctx.getDrawable(R.drawable.background_red));
        else if(item.getStatus().equals("низкий")) abc.setBackground(ctx.getDrawable(R.drawable.background_green));
        ((TextView) view1.findViewById(R.id.data)).setText(item.getTextData());
        ((TextView) view1.findViewById(R.id.priority)).setText(item.getPriority());
        ((TextView) view1.findViewById(R.id.statusView)).setText(item.getStatus());
        return view1;
    }
    ArrayList<Item> getItems(){
        ArrayList<Item> element = new ArrayList<Item>();
        for(Item i: items){
            element.add(i);
        }
        return element;
    }
}


