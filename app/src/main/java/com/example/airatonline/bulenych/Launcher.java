package com.example.airatonline.bulenych;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsMessage;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class Launcher extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(!registrated()){
            final SMSWorker smsWorker = new SMSWorker();
            setContentView(R.layout.activity_launcher);

            final EditText text= findViewById(R.id.editText);
            TextInputLayout Layout = findViewById(R.id.Layout);
            Layout.setHint("Введите номер телефона");
            Button btn = findViewById(R.id.button);
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                        smsWorker.sendSMS(text.getText().toString(), Launcher.this);
                }
            });}
        else{
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }
    }
    boolean registrated(){
        SharedPreferences sPref;
        String SAVED= "reg";
        sPref=getSharedPreferences("checkPhone", MODE_PRIVATE);

        if(!((sPref.getString("reg", "error")).equals("error"))){
            return true;
        }
        return false;
    }


}