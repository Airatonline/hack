package com.example.airatonline.bulenych;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.telephony.SmsMessage;
import android.util.Log;

import static android.content.Context.MODE_PRIVATE;
import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class SMSmonitor extends BroadcastReceiver {


    SmsMessage[] messages;

    @Override
    public void onReceive(Context context, Intent intent) {
        SMSWorker smsWorker = new SMSWorker();
        String SMS = smsWorker.getSMS(intent, context);
        Intent mIntent = new Intent(context, MainActivity.class);
        if(SMS.charAt(0)=='-' && SMS.length()==12){
            SharedPreferences sPref;
            String SAVED= "reg";
            sPref=context.getSharedPreferences("checkPhone", MODE_PRIVATE);
            SharedPreferences.Editor ed = sPref.edit();
            ed.putString("phone", SMS.substring(1));
            ed.commit();
        }
        else {
            SMSParser sms = new SMSParser();
            sms.SMSGet(SMS, context);
            mIntent.addFlags(FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(mIntent);
            abortBroadcast();
        }

    }
}

