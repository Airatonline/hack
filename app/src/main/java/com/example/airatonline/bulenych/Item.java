package com.example.airatonline.bulenych;

public class Item {
    private String ID;
    private String InfoWork;
    private String textData;
    private String priority;
    private String Status;

    public Item(String ID, String InfoWork, String textData, String Status, String priority){
        this.ID = ID;
        this.InfoWork = InfoWork;
        this.textData = textData;
        this.Status = Status;
        this.priority = priority;
    }
    public String getID(){
        return this.ID;
    }
    public void setID(String ID){
        this.ID = ID;
    }
    public String getInfoWork(){
        return this.InfoWork;
    }
    public void setInfoWork(String InfoWork){
        this.InfoWork = InfoWork;
    }

    public String getTextData() {
        return textData;
    }

    public void setTextData(String textData) {
        this.textData = textData;
    }

    public String getStatus(){
        return Status;
    }
    public void setStatus(String Status){
        this.Status = Status;
    }
    public String getPriority(){
        return priority;
    }
    public void setPriority(String priority){
        this.priority = priority;
    }
}
