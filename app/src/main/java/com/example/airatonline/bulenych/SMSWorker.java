package com.example.airatonline.bulenych;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.widget.Toast;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class SMSWorker {
    private static final String ACTION = "android.provider.Telephony.SMS_RECEIVED";
    String text_sms="";
    SmsMessage[] messages;


    String getSMS(Intent intent, Context context) {
        if (intent != null && intent.getAction() != null &&
                ACTION.compareToIgnoreCase(intent.getAction()) == 0) {
            Object[] pduArray = (Object[]) intent.getExtras().get("pdus");
           messages = new SmsMessage[pduArray.length];
            for (int i = 0; i < pduArray.length; i++) {
                messages[i] = SmsMessage.createFromPdu((byte[]) pduArray[i]);
            }
        }
        String sms_from = messages[0].getDisplayOriginatingAddress();
        StringBuilder bodyText = new StringBuilder();
        for (int i = 0; i < messages.length; i++) {
            bodyText.append(messages[i].getMessageBody());
        }
        text_sms = bodyText.toString();

        return text_sms;
    }
    void sendSMS(String text, Context context){
        SharedPreferences spref = context.getSharedPreferences("checkPhone", Context.MODE_PRIVATE);

        try {
            SmsManager.getDefault().sendTextMessage("+79393961768", null, text, null,null);
        }catch (Exception e){
            Toast.makeText(context, "Something was wrong", Toast.LENGTH_LONG);
        }
    }
}
