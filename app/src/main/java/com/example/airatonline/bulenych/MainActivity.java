package com.example.airatonline.bulenych;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.nhaarman.listviewanimations.appearance.simple.SwingBottomInAnimationAdapter;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    ListView task_list;
    Context context = this;
    DBHelper DBhelper;
    DataAdapter adapter=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        requestReadAndSendSmsPermission();
        isSmsPermissionGranted();
        DBhelper = new DBHelper(this);
        SMSParser parser = new SMSParser();
/*        parser.SMSGet("-0000-338-VSTREChA_S_ZhAVRUKOM&&-27_09_2018-SREDNIY-NE_NAChATA",context);
        parser.SMSGet("-0000-339-POVTORNO_VIhGRUZITJh_OTChETIh&&-21_09_2018-SREDNIY-NE_NAChATA", context);
        parser.SMSGet("-0000-340-OBRAJshENIE_VZJaTO_V_RABOTU&&-24_08_2018-SREDNIY-NE_NAChATA",context);
        parser.SMSGet("-0000-341-YYYYY&&-23_09_2018-SREDNIY-V_RABOTE",context);
        parser.SMSGet("-0000-342-OBRAJshENIE_ZAKRIhTO&&-01_08_2018-SREDNIY-NE_NAChATA",context);
        parser.SMSGet("-0000-343-OBRAJshENIE_RAZREShENO&&-30_07_2018-SREDNIY-NE_NAChATA",context);
        parser.SMSGet("-0000-344-OBRAJshENIE__RAZREShENO&&-18_09_2018-SREDNIY-NE_NAChATA",context);
        parser.SMSGet("-0000-345-OBRAJshENIE_VZJaTO_V_RABOTU&&-04_09_2018-SREDNIY-NE_NAChATA",context);
        parser.SMSGet("-0000-346-OBRAJshENIE_OTMENENO&&-29_07_2018-SREDNIY-NE_NAChATA",context);
        parser.SMSGet("-0000-347-PROVERITJh_ATTESTACIONNIhE_KEYSIh_UChASNIKOV_TRENINGA&&-24_09_2018-SREDNIY-NE_NAChATA",context);
        parser.SMSGet("-0000-348-RABOTA_NAD_INCIDENTOM_&&-19_09_2018-SREDNIY-V_RABOTE",context);
        parser.SMSGet("-0000-349-SOGLASOVATJh_S_JuRISTOM_DOGOVOR_NA_OBSLUZhIVANIE___ALJhFABIZNES_&&-25_09_2018-низкий-NE_NAChATA",context);
        parser.SMSGet("-0000-350-DIAGNOSTIROVATJh_I_REShITJh_INCIDENT_&&-09_09_2018-высокий-NE_NAChATA",context);*/

        task_list = findViewById(R.id.recyclerView);
        ArrayList<Item> elements = new ArrayList<Item>();
        //SMSParser parser1 = new SMSParser();

        SQLiteDatabase db = DBhelper.getReadableDatabase();
        try{
            Cursor c = db.query("task",null,null,null, null,null,"priority");
            String[] p = new String[5];
            if(c.moveToFirst()){
                int idCol = c.getColumnIndex("id");
                int textcol = c.getColumnIndex("text");
                int datacol = c.getColumnIndex("data");
                int prioritycol = c.getColumnIndex("priority");
                int satuscol = c.getColumnIndex("status");

                do{
                    p[0]=c.getString(idCol);
                    p[1]=c.getString(textcol);
                    p[2]=c.getString(datacol);
                    p[3]=c.getString(prioritycol);
                    p[4]=c.getString(satuscol);
                    elements.add(new Item(p[0], p[1], p[2], p[3], p[4]));
                } while(c.moveToNext());
                db.close();
            }
        }catch (Exception e){}





        adapter = new DataAdapter(this, elements);
        task_list.setAdapter(adapter);
        task_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(MainActivity.this, Send_result.class);
                intent.putExtra("ID", ((Item) adapter.getItem(i)).getID());
                intent.putExtra("Text", ((Item) adapter.getItem(i)).getInfoWork());
                intent.putExtra("Data", ((Item) adapter.getItem(i)).getTextData());
                intent.putExtra("Priority", ((Item) adapter.getItem(i)).getPriority());
                intent.putExtra("status", (((Item) adapter.getItem(i)).getStatus()));
                startActivity(intent);
            }
        });


    }


    public boolean isSmsPermissionGranted() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED;
    }
    private void requestReadAndSendSmsPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_SMS)) {
            // You may display a non-blocking explanation here, read more in the documentation:
            // https://developer.android.com/training/permissions/requesting.html
        }
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_SMS}, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==RESULT_OK){
            adapter.notifyDataSetChanged();
        }
    }
}
