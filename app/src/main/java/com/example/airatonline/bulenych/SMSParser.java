package com.example.airatonline.bulenych;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class SMSParser {
    DBHelper dbHelper;

    String[] SMSGet(String input, Context context){

        String[] output = new String[10];
        int j=0;
        int k=0;
        for(String i:input.split("-")) {
            if (j > 1) {
                output[k] = i;
                k++;
                Log.d("string ", i);
            }
            Log.d("string", i);
            j++;
        }

        dbHelper = new DBHelper(context);

        Log.d("SQL", "started");
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        try{
            dbHelper.onCreate(db);
        }catch (Exception e){}
        output[1]=SymbolReplace(output[1]);
        output[2]=ParceData(output[2]);
        output[3]=SymbolReplace(output[3]);
        output[4]=SymbolReplace(output[4]);

        ContentValues cv = new ContentValues();

        cv.put("text", output[1]);
        cv.put("data", output[2]);
        cv.put("priority", output[3]);
        cv.put("status", output[4]);
        try{
            db.update("task", cv, "id = ?", new String[]{output[0]});
        }catch (Exception e){}

        cv.put("id", output[0]);
        try{
            db.insert("task", null, cv);
        }catch (Exception e){
            dbHelper.onCreate(db);
        }

        return output;
    }

    String SymbolReplace(String s){
        s = s.replace("Je", "ё");
        s = s.replace("Zh", "ж");
        s = s.replace("Kh", "х");
        s = s.replace("Ch", "ч");
        s = s.replace("Sh", "ш");
        s = s.replace("Jsh", "щ");
        s = s.replace("Hh", "ъ");
        s = s.replace("Ih", "ы");
        s = s.replace("Jh", "ь");
        s = s.replace("Eh", "э");
        s = s.replace("Ju", "ю");
        s = s.replace("Ja", "я");
        s = s.replace("A", "а");
        s = s.replace("B", "б");
        s = s.replace("V", "в");
        s = s.replace("G", "г");
        s = s.replace("D", "д");
        s = s.replace("E", "е");
        s = s.replace("Z", "з");
        s = s.replace("I", "и");
        s = s.replace("Y", "й");
        s = s.replace("K", "к");
        s = s.replace("L", "л");
        s = s.replace("M", "м");
        s = s.replace("N", "н");
        s = s.replace("O", "о");
        s = s.replace("P", "п");
        s = s.replace("R", "р");
        s = s.replace("S", "с");
        s = s.replace("T", "т");
        s = s.replace("U", "у");
        s = s.replace("F", "ф");
        s = s.replace("C", "ц");
        s=s.replace("_", " ");
        return s;
    }
    String ParceData(String s){
        s=s.replace("_", ".");
        return s;
    }
}
